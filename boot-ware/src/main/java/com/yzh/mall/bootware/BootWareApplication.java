package com.yzh.mall.bootware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootWareApplication.class, args);
    }

}
