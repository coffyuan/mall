package com.yzh.mall.bootorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootOrderApplication.class, args);
    }

}
